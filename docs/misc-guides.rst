Miscellaneous guides
====================

These sections describe solutions to specific problems that don't quite fit
in other categories, around Cahute.

.. toctree::
    :maxdepth: 2

    misc-guides/get-started-with-cahute-in-visual-studio
    misc-guides/capture-usb-using-wireshark-on-windows
