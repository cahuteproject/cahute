.. _install-windows:

|win| Installing Cahute on Microsoft Windows
============================================

.. warning::

    Cahute does not provide any official methods of installation for Windows
    yet. See :ref:`system-windows` for more information.

    In the mean time, unofficial and possibly incomplete build methods are
    provided for Windows in :ref:`build-windows`.

.. |win| image:: win.png
