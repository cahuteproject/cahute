.. _install-amigaos:

|amigaos| Installing Cahute on AmigaOS
======================================

.. warning::

    Cahute does not provide any official methods of installation for AmigaOS
    yet. See :ref:`system-amigaos` for more information.

    In the mean time, unofficial and possibly incomplete build methods are
    provided for Windows in :ref:`build-amigaos`.

.. |amigaos| image:: amigaos.png
