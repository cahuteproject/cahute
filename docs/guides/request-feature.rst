.. _guide-request-feature:

Requesting a feature
====================

You may be needing a feature that is not yet present in Cahute, but you feel
is within its scope. In this case, the steps are the following:

* Go to the project's `issue tracker at Gitlab`_;
* Check if your feature has not already been requested. If it has, but it is
  lacking some of the details you have in mind, it is best to contribute on
  it directly, which may provide it a higher priority as suggests it is
  more requested;
* Otherwise, create an issue, while providing as many details as you can
  on the use case(s) of the given feature.

.. warning::

    In order to comment or create an issue, you will be required to have
    a `Gitlab.com`_ account. If you do not yet have one, you must
    `sign up on Gitlab.com`_ to continue.

.. warning::

    Once your feature request is up or sent, **please check on it every few
    weeks at least**, in order to be able to answer questions regarding it.

    A feature request made by someone who can't answer once additional details
    are required from them is a feature request that gets closed and has wasted
    everyone's time and efforts.

.. warning::

    Cahute is free software maintained by people on their free time, there is
    no guarantee of any delay, or even of a response or that the request
    won't be closed due to lack of availability on the maintainers' part.

    Note however that this warning is worst case scenario, and hopefully,
    it won't come to that for any correctly made feature request.

.. _Gitlab.com: https://about.gitlab.com/
.. _Sign up on Gitlab.com: https://gitlab.com/users/sign_up
.. _Issue tracker at Gitlab: https://gitlab.com/cahuteproject/cahute/-/issues
