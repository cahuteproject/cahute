.. _guide-install:

Installation guides
===================

These sections describe how to install Cahute on different platforms, using
different means.

.. toctree::
    :maxdepth: 2

    ../install-guides/linux
    ../install-guides/macos
    ../install-guides/windows
    ../install-guides/amigaos
