.. _protocol-ums:

USB Mass Storage (UMS)
======================

This protocol is used by all calculators starting from the fx-CG20, fx-CP400+E
and fx-9750GIII, when the "USB Key" mode is selected on the calculator.

It is present over :ref:`transport-ums`, and makes the following memories
available:

* Storage memory;
* Main memory, in the ``@MainMem`` directory at root.

.. todo:: Describe this protocol in more detail.
