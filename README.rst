Cahute -- Communication and file format handling tools for CASIO calculators
============================================================================

    **You may be viewing this through a mirror!**

    The official and main repository for issue / bug tracking, release notes,
    merge requests, and collaboration in general is at
    `gitlab.com/cahuteproject/cahute`_. As documented in the contribution
    guides listed below, all interactions must go through the official
    repository rather than its mirrors. Thanks!

Cahute is a library and set of command-line utilities to handle serial
and USB communication protocols and file formats related to CASIO calculators,
dating from the 1990s to today.

Cahute's officially supported features and systems list, as well as guides,
topics and references oriented towards users, developers, packagers and
contributors in general can be found in the documentation, depending on which
version you're using:

* `cahuteproject.org (latest release)`_;
* `next.cahuteproject.org (develop branch)`_.

Quick links to important guides to getting started with Cahute are the
following:

* `Installing Cahute`_;
* `Building Cahute from source`_;
* `Contributing to Cahute`_;
* `Reporting a bug or vulnerability`_;
* `Requesting a feature`_;
* `Packaging Cahute`_;
* `Creating a merge request`_.

The project’s code and documentation contents are licensed under CeCILL
version 2.1 as distributed by the CEA, CNRS and Inria on cecill.info.

For more information, consult the following links:

* `Releases`_, to list all releases of the project and access a human-curated
  changelog;
* `Community feedback`_, including presence on other websites.

Happy Cahuting!

.. _gitlab.com/cahuteproject/cahute: https://gitlab.com/cahuteproject/cahute
.. _next.cahuteproject.org (develop branch): https://next.cahuteproject.org/
.. _cahuteproject.org (latest release): https://cahuteproject.org/

.. _Installing Cahute: https://next.cahuteproject.org/guides/install.html
.. _Building Cahute from source:
    https://next.cahuteproject.org/guides/build.html
.. _Contributing to Cahute:
    https://next.cahuteproject.org/guides/contribute.html
.. _Reporting a bug or vulnerability:
    https://next.cahuteproject.org/guides/report.html
.. _Requesting a feature:
    https://next.cahuteproject.org/guides/request-feature.html
.. _Packaging Cahute: https://next.cahuteproject.org/guides/package.html
.. _Creating a merge request:
    https://next.cahuteproject.org/guides/create-merge-request.html

.. _CeCILL: http://www.cecill.info/licences.en.html
.. _cecill.info: http://www.cecill.info/licences.en.html

.. _Releases: https://gitlab.com/cahuteproject/cahute/-/releases
.. _Community feedback: https://next.cahuteproject.org/project/community.html
